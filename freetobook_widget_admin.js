(function ($) {

Drupal.freetobookWidget = {};

Drupal.freetobookWidget.toggleType = function(style)
{
  var display_images = (style == 'custom') ? 'block' : 'none';
  var display_buttons = (style == 'button') ? 'block' : 'none';
  $('#edit-freetobook-widget-image-fid-ajax-wrapper').css('display' , display_images);
  $('#freetobook-widget-buttons').css('display' , display_buttons);
}



Drupal.behaviors.freetobookWidget = {
  attach: function(context, settings) {
	Drupal.freetobookWidget.toggleType(Drupal.settings.freetobookWidget.style);
  }
  
  
}



}(jQuery));