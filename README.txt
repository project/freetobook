Freetobook booking button, designed for Drupal 7.x

Installation
------------

Copy the freetobook_widget folder to your module directory and then enable on the admin
modules page.  Enter your widget key on the module configuration page and save it.
Then, on the structure page, add the widget block where you want you your booking
button to appear.

Author
------
freetobook.com
drupal@freetobook.com
